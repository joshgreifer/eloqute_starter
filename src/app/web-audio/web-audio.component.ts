import { Component, OnInit, Input } from '@angular/core';
import { WebAudioService, AudioBufferWithLoadState } from '../webaudio.service';

@Component({
  selector: 'app-web-audio',
  templateUrl: './web-audio.component.html',
  styleUrls: ['./web-audio.component.css']
})
export class WebAudioComponent implements OnInit {

  procChain: AudioNode[];
  inputBuffer: AudioBufferWithLoadState;
  outputBuffer: AudioBufferWithLoadState;
  context: AudioContext;

  @Input()
  set Src(buf: AudioBufferWithLoadState) {
    this.inputBuffer = buf;

  }

  get Src(): AudioBufferWithLoadState {
    return  this.inputBuffer;
  }

  set Dest(buf: AudioBufferWithLoadState) {
    this.outputBuffer = buf;

  }

  get Dest(): AudioBufferWithLoadState {
    return  this.outputBuffer;
  }

  @Input()
  get HeadNode() {
    return  this.procChain[0];
  }

  @Input()
  get TailNode() {
    return  this.procChain[this.procChain.length - 1];
  }

  connectTo( next: WebAudioComponent) {
    this.TailNode.connect(next.HeadNode);

  }
  connectFrom( prev: WebAudioComponent) {
    prev.TailNode.connect(this.HeadNode);
  }

  constructor(public webAudioService: WebAudioService) {
    this.context = webAudioService.getContext();
    this.procChain = [];

  }

  ngOnInit() {
  }

}

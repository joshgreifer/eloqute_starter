import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { WebAudioService, AudioBufferWithLoadState } from '../webaudio.service';
import {WebAudioComponent} from '../web-audio/web-audio.component';

// export class AudioBufferWithLoadState {
//   url: string;
//   buffer: AudioBuffer;
//   state: string;
//
//   constructor(url: string) {
//     this.url = url;
//     this.buffer = null;
//     this.state = 'Not loaded';
//   }
// }

@Component({
  selector: 'app-url-audio-buffer',
  templateUrl: './url-audio-buffer.component.html',
  styleUrls: ['./url-audio-buffer.component.css'],
  providers: [{provide: WebAudioComponent, useExisting: forwardRef(() => UrlAudioBufferComponent)}]
})
export class UrlAudioBufferComponent extends WebAudioComponent implements OnInit {


  @Input()
  set SourceUrl(url: string) {
    this.inputBuffer = new AudioBufferWithLoadState(url);
    this.webAudioService.loadSound(this.inputBuffer);

  }

  get SourceUrl(): string {
    return  this.inputBuffer.url;
  }


  Play() {
    const b: AudioBufferSourceNode =  this.context.createBufferSource();
    b.buffer = this.inputBuffer.buffer;
    b.connect(this.context.destination);
    b.start();
  }

  constructor(public  webAudioService: WebAudioService) {
     super(webAudioService);

 //     [
      // new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/G4.mp3'),
      // new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/A4.mp3'),
 //     new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/A4_NOTFOUND.mp3')
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/C5.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/D5.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/E5.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/G5.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/A5.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/C6.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/D6.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/D%236.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/E6.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/G6.mp3'),
 //      new AudioBufferWithLoadState('https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/A6.mp3'),

//    ];

  }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlAudioBufferComponent } from './url-audio-buffer.component';

describe('UrlAudioBufferComponent', () => {
  let component: UrlAudioBufferComponent;
  let fixture: ComponentFixture<UrlAudioBufferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlAudioBufferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlAudioBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import {WebAudioComponent} from './web-audio/web-audio.component';
import {AudioRecorderComponent} from './audio-recorder/audio-recorder.component';
import {WaAnalyserNodeComponent} from './wa-analyser-node/wa-analyser-node.component';
import {UrlAudioBufferComponent} from './url-audio-buffer/url-audio-buffer.component';
import {WaMediaElementAudioSourceNodeComponent} from './wa-media-element-audio-source-node/wa-media-element-audio-source-node.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'eLoqute';
  url = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/G4.mp3';

  @ViewChildren(WebAudioComponent) viewChildren: QueryList<WebAudioComponent>;
  private components: Array<WebAudioComponent>;
  ngAfterViewInit() {
    // viewChildren is set

    this.components = this.viewChildren.toArray();
    const media_element_audio_source: WaMediaElementAudioSourceNodeComponent = <WaMediaElementAudioSourceNodeComponent>this.components[0];
    // const audio_recorder: AudioRecorderComponent = <AudioRecorderComponent>this.components[1];
    const analyser: WaAnalyserNodeComponent = <WaAnalyserNodeComponent>this.components[1];
    navigator.getUserMedia({ audio: true }, (stream: MediaStream) => {
      // console.log(audio_recorder.TailNode);
      console.log(analyser.HeadNode);
      // audio_recorder.connectTo(analyser);
      media_element_audio_source.connectTo(analyser);
    }, ( err ) => { console.log('The following gUM error occurred: ' + err); });
    }
}

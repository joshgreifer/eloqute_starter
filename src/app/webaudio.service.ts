import { Injectable } from '@angular/core';

export class AudioBufferWithLoadState {
  url: string;
  buffer: AudioBuffer;
  state: string;

  constructor(url: string) {
    this.url = url;
    this.buffer = null;
    this.state = 'Not loaded';
  }

}

@Injectable()
export class WebAudioService {
  private context: AudioContext;
  constructor() {  this.context = new AudioContext; }

  getContext(): AudioContext { return this.context; }

  loadSound(b: AudioBufferWithLoadState) {
    const request = new XMLHttpRequest();
    request.open('get', b.url, true);

    request.responseType = 'arraybuffer';
    request.onerror = () => {
      b.state  = request.statusText;
    };
    request.onload = () => {
      b.state = 'Loaded';
      if (request.status === 200) {
        if (this.context) {
          this.context.decodeAudioData(request.response).then((buffer: AudioBuffer) => {
            b.buffer = buffer;
            b.state = 'Decoded';
          });
        } else {
          b.state = 'No AudioContext';
        }
      } else {
        b.state  = request.statusText;
      }
    };
    request.send();
    b.state = 'Loading...';
  }
}

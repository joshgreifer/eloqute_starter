import { Component, OnInit, Input, AfterViewInit, ViewChild, forwardRef } from '@angular/core';
import { WebAudioService } from '../webaudio.service';
import {WebAudioComponent} from '../web-audio/web-audio.component';


@Component({
  selector: 'app-wa-analyser-node',
  templateUrl: './wa-analyser-node.component.html',
  styleUrls: ['./wa-analyser-node.component.css'],
  providers: [{provide: WebAudioComponent, useExisting: forwardRef(() => WaAnalyserNodeComponent)}]

})
export class WaAnalyserNodeComponent  extends WebAudioComponent implements OnInit, AfterViewInit {

  @ViewChild('fft_canvas') fft_canvas_ref;
  private fft_canvas: HTMLCanvasElement;
  private fft_canvas_context: CanvasRenderingContext2D;

  private analyser: AnalyserNode;

  private animation_request_id;

  constructor(public  webAudioService: WebAudioService) {
    super(webAudioService);
    this.analyser = this.context.createAnalyser();
    this.procChain.push(this.analyser);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  // fft_canvas is set

    this.fft_canvas = this.fft_canvas_ref.nativeElement;
    this.fft_canvas_context = this.fft_canvas.getContext('2d');
    this.visualize();
  }

  visualize() {

    const WIDTH = this.fft_canvas.width;
    const HEIGHT = this.fft_canvas.height;
    const analyser = this.analyser;
    const canvasCtx = this.fft_canvas_context;

    analyser.fftSize = 2048;
    analyser.maxDecibels = -20.0;
    analyser.minDecibels =  -80.0;

    const bufferLengthAlt = analyser.frequencyBinCount;
    const dataArrayAlt = new Uint8Array(bufferLengthAlt);

      canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

    const drawAlt = () => {
        this.animation_request_id = requestAnimationFrame(drawAlt);

        analyser.getByteFrequencyData(dataArrayAlt);

        canvasCtx.fillStyle = 'rgb(0, 0, 0)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

        const barWidth = (WIDTH / bufferLengthAlt) * 2.5;
        let barHeight;
        let x = 0;

        for ( let i = 0; i < bufferLengthAlt; i++) {
          barHeight = dataArrayAlt[i];

          canvasCtx.fillStyle = 'rgb(' + (barHeight + 100) + ',50,50)';
          canvasCtx.fillRect(x, HEIGHT - barHeight / 2, barWidth, barHeight / 2);

          x += barWidth + 1;
        }
      };

      drawAlt();


  }
}

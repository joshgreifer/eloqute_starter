import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaAnalyserNodeComponent } from './wa-analyser-node.component';

describe('WaAnalyserNodeComponent', () => {
  let component: WaAnalyserNodeComponent;
  let fixture: ComponentFixture<WaAnalyserNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaAnalyserNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaAnalyserNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

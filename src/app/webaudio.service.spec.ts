import { TestBed, inject } from '@angular/core/testing';

import { WebAudioService } from './webaudio.service';

describe('WebAudioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebAudioService]
    });
  });

  it('should be created', inject([WebAudioService], (service: WebAudioService) => {
    expect(service).toBeTruthy();
  }));
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { UrlAudioBufferComponent } from './url-audio-buffer/url-audio-buffer.component';
import { WebAudioService } from './webaudio.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AudioRecorderComponent } from './audio-recorder/audio-recorder.component';
import { WebAudioComponent } from './web-audio/web-audio.component';
import { WaAnalyserNodeComponent } from './wa-analyser-node/wa-analyser-node.component';
import { WaMediaElementAudioSourceNodeComponent } from './wa-media-element-audio-source-node/wa-media-element-audio-source-node.component';

@NgModule({
  declarations: [
    AppComponent,
    UrlAudioBufferComponent,
    AudioRecorderComponent,
    WebAudioComponent,
    WaAnalyserNodeComponent,
    WaMediaElementAudioSourceNodeComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpClientModule
  ],
  providers: [WebAudioService],
  bootstrap: [AppComponent]
})
export class AppModule { }

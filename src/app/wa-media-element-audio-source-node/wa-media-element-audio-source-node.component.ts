import { Component, OnInit, Input, AfterViewInit, ViewChild, forwardRef } from '@angular/core';
import { WebAudioService } from '../webaudio.service';
import {WebAudioComponent} from '../web-audio/web-audio.component';


@Component({
  selector: 'app-wa-media-element-audio-source-node',
  templateUrl: './wa-media-element-audio-source-node.component.html',
  styleUrls: ['./wa-media-element-audio-source-node.component.css'],
  providers: [{provide: WebAudioComponent, useExisting: forwardRef(() => WaMediaElementAudioSourceNodeComponent)}]

})
export class WaMediaElementAudioSourceNodeComponent  extends WebAudioComponent implements OnInit, AfterViewInit {

  url: string;

  @ViewChild('audio') media_ref;
  @ViewChild('src') src_ref;
  private media: HTMLMediaElement;
  private source: HTMLSourceElement;

  private node: MediaElementAudioSourceNode;

  @Input()
  set SourceUrl(url: string) {
     this.url = url;
  }
  get SourceUrl(): string {
    return this.url;
   }


  constructor(public  webAudioService: WebAudioService) {
    super(webAudioService);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // fft_canvas is set

    this.media = this.media_ref.nativeElement;
    this.source = this.src_ref.nativeElement;
    this.source.src = this.url;
    this.node = this.context.createMediaElementSource(this.media);

    this.node.connect(this.context.destination);
    this.procChain.push(this.node);
   }

}

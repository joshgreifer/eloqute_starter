import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaMediaElementAudioSourceNodeComponent } from './wa-media-element-audio-source-node.component';

describe('WaMediaElementAudioSourceNodeComponent', () => {
  let component: WaMediaElementAudioSourceNodeComponent;
  let fixture: ComponentFixture<WaMediaElementAudioSourceNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaMediaElementAudioSourceNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaMediaElementAudioSourceNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

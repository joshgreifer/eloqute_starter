import { Component, OnInit, NgZone, forwardRef } from '@angular/core';
import { WebAudioService, AudioBufferWithLoadState } from '../webaudio.service';
import {WebAudioComponent} from '../web-audio/web-audio.component';
interface RecorderJS {
  node: ScriptProcessorNode;

  record(): void;
  stop(): void;
  exportWAV(callback: (audio_data: Blob) => any): boolean;

}

declare var Recorder: any;

@Component({
  selector: 'app-audio-recorder',
  templateUrl: './audio-recorder.component.html',
  styleUrls: ['./audio-recorder.component.css'],
  providers: [{provide: WebAudioComponent, useExisting: forwardRef(() => AudioRecorderComponent)}]
})
export class AudioRecorderComponent extends WebAudioComponent implements OnInit, WebAudioComponent {
  private sourceNode: MediaStreamAudioSourceNode;
  private recorder: RecorderJS;


  constructor(webAudioService_: WebAudioService, private ngZone_: NgZone) {
    super(webAudioService_);
    this.outputBuffer = new AudioBufferWithLoadState('');
    this.sourceNode = null;

    navigator.getUserMedia({ audio: true }, (stream: MediaStream) => {
      this.sourceNode = this.context.createMediaStreamSource(stream);
      this.recorder = <RecorderJS>new Recorder(this.sourceNode);
      this.procChain.push(this.recorder.node);
    }, ( err ) => { console.log('The following gUM error occurred: ' + err); });
  }

  record() {
    this.recorder.record();
  }
  stop() {
    this.recorder.stop();
    this.recorder.exportWAV((audio_data) => {
      this.ngZone_.run(() => {
        this.outputBuffer = new AudioBufferWithLoadState(URL.createObjectURL(audio_data));
      });
    });
  }

  ngOnInit() {

  }

}
